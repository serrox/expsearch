﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;

namespace expSearch
{
    [Serializable]
    struct ParamsFile
    {
        public string directory;
        public string filename;
        public string textinfile;
    }

    class SearchClass
    {
        public  string           directory = "";
        public  string           filename = "";
        public  string           textInFile = "";

        private BackgroundWorker   Worker;
        private SearchingFilesTree tree;

        // user's search params saved in file as serialized object of ParamsFile structure
        public void SaveParams()
        {
            ParamsFile f = new ParamsFile();
            f.directory  = directory;
            f.filename   = filename;
            f.textinfile = textInFile;
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream("params.dat", FileMode.OpenOrCreate);
            formatter.Serialize(fs, f);
            fs.Close();
        }

        public void LoadParams()
        {
            FileStream fs = new FileStream("params.dat", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            ParamsFile f;
            try
            {
                f = (ParamsFile)formatter.Deserialize(fs);
            }
            catch(Exception)
            {
                fs.Close();
                return;
            }
            fs.Close();
            directory  = f.directory;
            filename   = f.filename;
            textInFile = f.textinfile;
        }

        // starts search
        public void Search(ProgressChangedEventHandler ProgressChangedHandler, RunWorkerCompletedEventHandler RunWorkerCompletedHandler, bool newSearch)
        {
            if(directory == "" || !Directory.Exists(directory))
            {
                throw new InvalidDataException();
            }

            if (newSearch)
            {
                tree = new SearchingFilesTree(directory, true);
            }

            //do search in other thread
            Worker = new BackgroundWorker();
            Worker.WorkerReportsProgress = true;
            Worker.WorkerSupportsCancellation = true;
            Worker.DoWork += new DoWorkEventHandler(SearchWorker);
            Worker.ProgressChanged += ProgressChangedHandler;
            Worker.RunWorkerCompleted += RunWorkerCompletedHandler;

            Worker.RunWorkerAsync();
        }

        public void StopSearch()
        {
            Worker.CancelAsync();
        }

        // Main of searching thread
        private void SearchWorker(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker thisworker = sender as BackgroundWorker;

            // do search recursievly
            doSearchWork(thisworker, directory, tree);

            //check for Cancel request - it happens when user asks pause
            if (thisworker.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            // done.
            tree.visited = true;
        }

        // Searching function. uses recursion
        private void doSearchWork(BackgroundWorker thisworker, string dir, SearchingFilesTree current)
        {
            string[] searchdirectory;
            string[] files;

            try
            {
                searchdirectory = Directory.GetDirectories(dir);
            }
            catch (UnauthorizedAccessException)
            {
                // we do not have right to access this folder
                return;
            }
            if (searchdirectory.Length > 0)
            {
                for (int i = 0; i < searchdirectory.Length; i++)
                {
                    // skip folders we have visited before pause
                    if (!current.hasVisitedChild(searchdirectory[i], true))
                    {
                        SearchingFilesTree newFolder = current.addChild(searchdirectory[i], true);
                        doSearchWork(thisworker, searchdirectory[i], newFolder);
                        //mark folder visited only when we have visited all it's file and
                        newFolder.visited = true;

                        //check for Cancel request - it happens when user asks pause
                        if (thisworker.CancellationPending)
                            return;
                    }
                }
            }

            // Search files using given pattern
            // pattern may be not pattern itself, but part of file name
            files = Directory.GetFiles(dir, "*" + filename + "*");

            // ReportProgress with 0 means we working on this file now. Used to show which file is under work
            // ReportProgress with 1 means that we found one of files we search.
            // ReportProgress with 2 means that we checked file with no proplem. Used to count files.
            for (int i = 0; i < files.Length; i++)
            {
                // skip files we have visited before pause
                if (!current.hasVisitedChild(files[i], false))
                {
                    SearchingFilesTree newFile = current.addChild(files[i], false);
                    try
                    {
                        thisworker.ReportProgress(0, newFile);
                        if (textInFile != "")
                        {
                            if (File.ReadAllText(files[i]).Contains(textInFile))
                                thisworker.ReportProgress(1, newFile);
                        }
                        else
                        {
                            thisworker.ReportProgress(1, newFile);
                            //we adding found files too fast for gui - it hangs! add a small delay to prevent this
                            Thread.Sleep(1);
                            // double delay to give a chance for timer to work. we are still working too fast, but with this pause timer freezes very rare and anlo for 5-10 seconds as for me.
                            Thread.Sleep(1);
                        }
                        //do not send file representation - this call is used only for file counting
                        thisworker.ReportProgress(2, null);
                        newFile.visited = true;
                    }
                    catch (OutOfMemoryException)
                    {
                        // do not count this file since we can't check it!
                        MessageBox.Show("Недостаточно памяти для обработки файла " + files[i] + ".\n Поиск будет продолжен без учёта этого файла!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        // mark file as visited - don't try to check it again after search re-start
                        newFile.visited = true;
                        continue;
                    }
                    catch (UnauthorizedAccessException)
                    {
                        // we do not have rights to access this file, so skip it and mark it visited - don't try to check it again after search re-start
                        // do not count this file since we can't check it!
                        newFile.visited = true;
                        continue;
                    }
                }

                //check for Cancel request - it happens when user asks pause
                if (thisworker.CancellationPending)
                    return;
            }
        }
    }
}
