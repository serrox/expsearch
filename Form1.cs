﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace expSearch
{
    public partial class Form1 : Form
    {
        SearchClass engine;
        ulong       countOfFiles;
        Timer       timer;
        DateTime    started;
        bool        paused;
        TimeSpan    previousStarttime;

        public Form1()
        {
            engine = new SearchClass();
            engine.LoadParams();
            InitializeComponent();
            textBox1.Text = engine.directory;
            textBox2.Text = engine.filename;
            textBox3.Text = engine.textInFile;
        }

        private void buttonChooseFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = fbd.SelectedPath;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            engine.directory = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            engine.filename = textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            engine.textInFile = textBox3.Text;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            engine.SaveParams();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            engine.SaveParams();
            labelCountOfFiles.Text = "Обработанно файлов: 0";
            countOfFiles = 0;
            labelTime.Text = "Прошло времени 00:00:00";
            started = DateTime.Now;
            previousStarttime = new TimeSpan(0, 0, 0);
            timer = new Timer();
            timer.Interval = 1000; // 1 sec
            timer.Tick += Timer_Tick;
            timer.Start();
            treeView1.Nodes.Add(textBox1.Text);
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            buttonChooseFolder.Enabled = false;
            buttonSearch.Enabled = false;
            buttonPause.Enabled = true;
            buttonNewSearch.Enabled = false;
            paused = false;
            try
            {
                engine.Search(new ProgressChangedEventHandler(SearchProgressChanged), new RunWorkerCompletedEventHandler(SearchEnded), true);
            }
            catch (InvalidDataException)
            {
                timer.Stop();
                resetSearch();
                MessageBox.Show("Указанной папки не существует.\nПроверьте правильность ввода пути или использыйде диалог выбора папки", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            TimeSpan timeUsed = DateTime.Now - started + previousStarttime;
            labelTime.Text = "Прошло времени " + (timeUsed.Hours > 9 ? timeUsed.Hours.ToString() : "0" + timeUsed.Hours) + ":" + (timeUsed.Minutes > 9 ? timeUsed.Minutes.ToString() : "0" + timeUsed.Minutes) + ":" + (timeUsed.Seconds > 9 ? timeUsed.Seconds.ToString() : "0" + timeUsed.Seconds);
        }

        private void SearchEnded(object sender, RunWorkerCompletedEventArgs e)
        {
            timer.Stop();
            if (!paused)
            {
                buttonPause.Enabled = false;
            }
            TimeSpan timeUsed = DateTime.Now - started + previousStarttime;
            previousStarttime = timeUsed;
            labelTime.Text = "Прошло времени " + (timeUsed.Hours > 9 ? timeUsed.Hours.ToString() : "0" + timeUsed.Hours) + ":" + (timeUsed.Minutes > 9 ? timeUsed.Minutes.ToString() : "0" + timeUsed.Minutes) + ":" + (timeUsed.Seconds > 9 ? timeUsed.Seconds.ToString() : "0" + timeUsed.Seconds);
            buttonNewSearch.Enabled = true;
        }

        private void SearchProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 0)
            {
                labelCurrentFile.Text = (e.UserState as SearchingFilesTree).fpath;
            }
            else if(e.ProgressPercentage == 1)
            {
                TreeNode treeNode = treeView1.Nodes[0];
                AddFileToTreeView(e.UserState as SearchingFilesTree, ref treeNode);
            }
            else if(e.ProgressPercentage == 2)
            {
                countOfFiles++;
                labelCountOfFiles.Text = "Обработанно файлов: " + countOfFiles;
            }
        }

        private void AddFileToTreeView(SearchingFilesTree file, ref TreeNode node)
        {
            if (file.parent != null)
            {
                bool hasSuchNode = false;
                AddFileToTreeView(file.parent,ref node);
                TreeNode ourNode = null;
                foreach (TreeNode tn in node.Nodes)
                {
                    if(tn.Text == file.name)
                    {
                        hasSuchNode = true;
                        ourNode = tn;
                        break;
                    }
                }
                if(!hasSuchNode)
                {
                    ourNode = node.Nodes.Add(file.name);
                }
                node = ourNode;
            }
        }

        private void buttonNewSearch_Click(object sender, EventArgs e)
        {
            resetSearch();
        }

        private void resetSearch()
        {
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            buttonChooseFolder.Enabled = true;
            buttonSearch.Enabled = true;
            buttonPause.Enabled = false;
            labelTime.Text = "";
            labelCountOfFiles.Text = "";
            labelCurrentFile.Text = "";
            treeView1.Nodes.Clear();
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            if (paused)
            {
                buttonNewSearch.Enabled = false;
                paused = false;
                buttonPause.Text = "Пауза";
                started = DateTime.Now;
                timer.Start();
                engine.Search(new ProgressChangedEventHandler(SearchProgressChanged), new RunWorkerCompletedEventHandler(SearchEnded), false);
            }
            else
            {
                paused = true;
                buttonPause.Text = "Продолжить";
                engine.StopSearch();
            }
        }

        private void labelCurrentFile_SizeChanged(object sender, EventArgs e)
        {
            if(labelCurrentFile.Height > labelCurrentFile.MinimumSize.Height)
            {
                labelCurrentFile.MinimumSize = new Size(labelCurrentFile.Width, labelCurrentFile.Height);
            }
        }
    }
}
