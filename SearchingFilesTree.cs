﻿using System.Collections.Generic;
using System.IO;

namespace expSearch
{
    class SearchingFilesTree
    {
        public string fpath    { get; private set; } = "";
        public string name     { get; private set; } = "";
        public bool   visited                        = false ;
        public bool   isFolder { get; private set; }

        public  SearchingFilesTree       parent   { get; private set; } = null;
        private List<SearchingFilesTree> children                       = new List<SearchingFilesTree>();

        public SearchingFilesTree(string inpath, bool Folder)
        {
            realConstructor(inpath, Folder, null);
        }

        private SearchingFilesTree(string inpath, bool Folder, SearchingFilesTree inparent)
        {
            realConstructor(inpath, Folder, inparent);
        }

        private void realConstructor(string inpath, bool Folder, SearchingFilesTree inparent)
        {

            fpath = inpath;
            isFolder = Folder;
            parent = inparent;
            string t = Path.GetFileName(inpath);
            if (t == string.Empty || t == null)
            {
                t = new DirectoryInfo(inpath).Name;
            }
            name = t;
        }

        public SearchingFilesTree addChild(string inpath, bool Folder)
        {
            SearchingFilesTree newSFT = new SearchingFilesTree(inpath, Folder, this);
            children.Add(newSFT);
            return newSFT;
        }

        public bool hasVisitedChild(string inpath, bool Folder)
        {
            foreach (SearchingFilesTree child in children)
            {
                if(child.visited == true && child.fpath == inpath && child.isFolder == Folder)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
